package pe.edu.uni.fiis.lasBrujasDelBlair.service.producto;

import pe.edu.uni.fiis.lasBrujasDelBlair.model.Producto;

import java.sql.Connection;
import java.util.List;

public interface ProductoService {
    public boolean agregarProducto(Producto a);
    public boolean agregarProductoCategoria(Producto a);
    public List<Producto> obtenerProductos();
}
