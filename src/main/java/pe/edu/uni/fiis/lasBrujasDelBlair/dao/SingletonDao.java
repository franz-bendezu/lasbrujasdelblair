package pe.edu.uni.fiis.lasBrujasDelBlair.dao;


import pe.edu.uni.fiis.lasBrujasDelBlair.dao.categoria.CategoriaDao;
import pe.edu.uni.fiis.lasBrujasDelBlair.dao.categoria.CategoriaDaoImpl;
import pe.edu.uni.fiis.lasBrujasDelBlair.dao.pedido.PedidoDao;
import pe.edu.uni.fiis.lasBrujasDelBlair.dao.pedido.PedidoDaoImpl;
import pe.edu.uni.fiis.lasBrujasDelBlair.dao.producto.ProductoDao;
import pe.edu.uni.fiis.lasBrujasDelBlair.dao.producto.ProductoDaoImpl;
import pe.edu.uni.fiis.lasBrujasDelBlair.dao.usuariof.UsuarioFDao;
import pe.edu.uni.fiis.lasBrujasDelBlair.dao.usuariof.UsuarioFDaoImpl;

public abstract class SingletonDao {

    private static UsuarioFDao usuarioFDao = null;
    public static UsuarioFDao getUsuarioFDao(){
        if(usuarioFDao == null){
            usuarioFDao = new UsuarioFDaoImpl();
        }
        return usuarioFDao;
    }

    private static PedidoDao pedidoDao = null;
    public static PedidoDao getPedidoDao() {
        if (pedidoDao == null) {
            pedidoDao = new PedidoDaoImpl();
        }
        return pedidoDao;
    }

    private static ProductoDao productoDao = null;
    public static ProductoDao getProductoDao() {
        if (productoDao == null) {
            productoDao = new ProductoDaoImpl();
        }
        return productoDao;
    }

    private static CategoriaDao categoriaDao = null;
    public static CategoriaDao getCategoriaDao() {
        if (categoriaDao == null) {
            categoriaDao = new CategoriaDaoImpl();
        }
        return categoriaDao;
    }
}