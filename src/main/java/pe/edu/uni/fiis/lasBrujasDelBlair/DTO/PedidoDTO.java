package pe.edu.uni.fiis.lasBrujasDelBlair.DTO;


import pe.edu.uni.fiis.lasBrujasDelBlair.model.Pedido;

import java.util.List;

public class PedidoDTO {
    private List<Pedido> pedidos;

    public List<Pedido> getPedidos() {
        return pedidos;
    }

    public void setPedidos(List<Pedido> pedidos) {
        this.pedidos = pedidos;
    }
}
