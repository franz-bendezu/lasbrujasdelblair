package pe.edu.uni.fiis.lasBrujasDelBlair.service.categoria;


import pe.edu.uni.fiis.lasBrujasDelBlair.model.Categoria;

import java.util.List;

public interface CategoriaService {
    public List<Categoria> obtenerCategorias();
    public Categoria actualizarCategorias(Categoria categoria);
}
