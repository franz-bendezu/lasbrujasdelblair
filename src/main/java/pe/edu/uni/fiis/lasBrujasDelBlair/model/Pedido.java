package pe.edu.uni.fiis.lasBrujasDelBlair.model;

import java.util.List;

public class Pedido {
    private String idPedido;
    private UsuarioF usuarioF;
    private List<Producto> productos;
    private Categoria categoria;
    private Integer cantidad;

    public String getIdPedido() {
        return idPedido;
    }

    public void setIdPedido(String idPedido) {
        this.idPedido = idPedido;
    }

    public UsuarioF getUsuarioF() {
        return usuarioF;
    }

    public void setUsuarioF(UsuarioF usuarioF) {
        this.usuarioF = usuarioF;
    }

    public List<Producto> getProductos() {
        return productos;
    }

    public void setProductos(List<Producto> productos) {
        this.productos = productos;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }


    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }
}


