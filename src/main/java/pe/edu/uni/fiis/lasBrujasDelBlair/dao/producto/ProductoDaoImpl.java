package pe.edu.uni.fiis.lasBrujasDelBlair.dao.producto;

import pe.edu.uni.fiis.lasBrujasDelBlair.model.Categoria;
import pe.edu.uni.fiis.lasBrujasDelBlair.model.Producto;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProductoDaoImpl implements ProductoDao{
    public boolean agregarProducto(Producto a, Connection b) {
        boolean c=false;
        try {
            StringBuffer sql = new StringBuffer();
            sql.append("insert into producto( id_producto, nombre, tipo) values (?,?,?);");
            PreparedStatement sentencia = b.prepareStatement(sql.toString());
            sentencia.setString(1,a.getIdProducto());
            sentencia.setString(2,a.getNombre());
            sentencia.setString(3,a.getTipo());
            int d= sentencia.executeUpdate();
            c=d>0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return c;
    }
    public boolean agregarProductoCategoria(Producto a, Connection b) {
        boolean c=false;
        try {
            StringBuffer sql = new StringBuffer();
            sql.append("insert into producto_categoria( id_producto, id_categoria ," +
                    "id_producto_categoria , cantidad) values (?,?,?,?); ");
            PreparedStatement sentencia = b.prepareStatement(sql.toString());
            sentencia.setString(1,a.getIdProducto());
            sentencia.setString(2,a.getCategoria().getNombre());
            sentencia.setString(3,a.getIdProducto()+a.getCategoria().getNombre());
            sentencia.setInt(4,a.getCantidad());
            int d= sentencia.executeUpdate();
            c=d>0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return c;
    }


    public List<Producto> obtenerProductos(Connection b) {
        List<Producto> lista = new ArrayList<Producto>();
        try {
            StringBuffer sql = new StringBuffer();
            sql.append(" select pro.id_producto, pro.nombre, " +
                    " pro.tipo,cat.id_categoria, " +
                    " pro_cat.cantidad "+
                    " from producto pro" +
                    " inner join producto_categoria pro_cat " +
                    " on (pro.id_producto=pro_cat.id_producto)" +
                    " inner join categoria cat" +
                    " on (pro_cat.id_categoria=cat.id_categoria);");
            Statement sentencia = b.createStatement();
            ResultSet rs = sentencia.executeQuery(sql.toString());

            while (rs.next()){
                Producto u = new Producto();
                u.setIdProducto(rs.getString("id_producto"));
                u.setNombre(rs.getString("nombre"));
                u.setCategoria(new Categoria());
                u.getCategoria().setIdCategoria(rs.getString("id_categoria"));
                u.setTipo(rs.getString("tipo"));
                u.setCantidad(rs.getInt("cantidad"));
                lista.add(u);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lista;
    }
}
