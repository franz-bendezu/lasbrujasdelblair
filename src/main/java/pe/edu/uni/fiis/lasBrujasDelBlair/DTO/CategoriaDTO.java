package pe.edu.uni.fiis.lasBrujasDelBlair.DTO;

import pe.edu.uni.fiis.lasBrujasDelBlair.model.Categoria;

import java.util.List;

public class CategoriaDTO {
    private List<Categoria> categorias;

    public List<Categoria> getCategorias() {
        return categorias;
    }

    public void setCategorias(List<Categoria> categorias) {
        this.categorias = categorias;
    }
}
